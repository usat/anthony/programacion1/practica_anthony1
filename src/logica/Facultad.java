/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 *
 * @author silviopd
 */
public class Facultad extends Conexion{
    
    private int id_universidad,id_facultad;
    private String nombre,estado;

    public int getId_universidad() {
        return id_universidad;
    }

    public void setId_universidad(int id_universidad) {
        this.id_universidad = id_universidad;
    }

    public int getId_facultad() {
        return id_facultad;
    }

    public void setId_facultad(int id_facultad) {
        this.id_facultad = id_facultad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    
    public ResultSet listar() throws Exception {
        String sql = "select * from facultad";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        return resultado;
    }

    public boolean agregar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "INSERT INTO public.facultad(\n" +
"           nombre, id_universidad, estado)\n" +
"    VALUES (?, ?, ?);";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getNombre());
            spInsertar.setInt(2, this.getId_universidad());
            spInsertar.setString(3, this.getEstado());
            this.ejecutarSQL(spInsertar,transaccion);
            
            transaccion.commit();
            transaccion.close();

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public ResultSet leerDatosCodigo(int dni) throws Exception {
        String sql = "select * from facultad where id_facultad = ?";
        PreparedStatement sentencia = abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, dni);
        return ejecutarSQL(sentencia);
    }

    public boolean editar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "UPDATE public.facultad\n" +
"   SET nombre=?, id_universidad=?, estado=?\n" +
" WHERE id_facultad=?;";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getNombre());
            spInsertar.setInt(2, this.getId_universidad());
            spInsertar.setString(3, this.getEstado());
            spInsertar.setInt(4, this.getId_facultad());
            
            this.ejecutarSQL(spInsertar,transaccion);
            
            transaccion.commit();
            transaccion.close();

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public boolean eliminar() throws Exception {
        Connection transaccion = abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "delete from facultad where id_facultad= ?";

        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setInt(1, getId_facultad());

        this.ejecutarSQL(sentencia, transaccion);
        transaccion.commit();
        transaccion.close();

        this.cerrarConexion(transaccion);

        return true;
    }
    
}
