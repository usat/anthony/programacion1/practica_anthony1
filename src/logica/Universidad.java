/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 *
 * @author silviopd
 */
public class Universidad extends Conexion{
    
    private int id_universidad;
    private String nombre;
    
    public static ArrayList<Universidad> listarUniversidad= new ArrayList<Universidad>();

    public int getId_universidad() {
        return id_universidad;
    }

    public void setId_universidad(int id_universidad) {
        this.id_universidad = id_universidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public ResultSet listar() throws Exception {
        String sql = "select * from universidad";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        return resultado;
    }

    public boolean agregar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "select * from fn_universidad_agregar(?)";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getNombre());
            ResultSet resultado = this.ejecutarSQL2(spInsertar);

            if (resultado.next()) {
                transaccion.commit();
            } else {
                transaccion.rollback();
            }

            transaccion.close();
            this.cerrarConexion(transaccion);

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public ResultSet leerDatosCodigo(int dni) throws Exception {
        String sql = "select * from universidad where id_universidad = ?";
        PreparedStatement sentencia = abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, dni);
        return ejecutarSQL(sentencia);
    }

    public boolean editar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "UPDATE public.universidad\n" +
"   SET nombre=?\n" +
" WHERE id_universidad=?;";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getNombre());
            spInsertar.setInt(2, this.getId_universidad());
            
            this.ejecutarSQL(spInsertar,transaccion);
            
            transaccion.commit();
            transaccion.close();

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public boolean eliminar() throws Exception {
        Connection transaccion = abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "delete from universidad where id_universidad = ?";

        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setInt(1, getId_universidad());

        this.ejecutarSQL(sentencia, transaccion);
        transaccion.commit();
        transaccion.close();

        this.cerrarConexion(transaccion);

        return true;
    }
    
    /************************************/
    
    private void cargarDatos() throws Exception
    {
        String sql = "select * from universidad";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        listarUniversidad.clear();
        while(resultado.next())
        {
            Universidad objMarca = new Universidad();
            objMarca.setId_universidad(resultado.getInt("id_universidad"));
            objMarca.setNombre(resultado.getString("nombre"));
            listarUniversidad.add(objMarca);
        }
    }
    
    public void llenarComboAutos(JComboBox combo) throws Exception
    {
        this.cargarDatos();
        combo.removeAllItems();
        for(int i = 0; i<listarUniversidad.size(); i++)
        {
            Universidad m = listarUniversidad.get(i);
            combo.addItem(m.getNombre());
        }
    }
}
