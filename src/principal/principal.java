/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import javax.swing.UIManager;
import presentacion.FrmMenuPrincipal;

/**
 *
 * @author silviopd
 */
public class principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            System.out.println("Ocurrió un error al cargar el tema");
        }

        //Instancia el formulario
        // FrmIniciarSesion objFrm = new FrmIniciarSesion();
        FrmMenuPrincipal objFrm = new FrmMenuPrincipal();
        //Muestra el formulario en pantalla
        objFrm.setVisible(true);

    
    }
    
}
